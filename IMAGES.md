# Images

- **Bubble sort**

![Bubble sort](https://github.com/hpedrorodrigues/Sort/blob/master/images/BubbleSort.gif)

<!---
- **Bubble sort with flag**

 ![Bubble sort with flag]()

- **Counting sort**

![Counting sort]()

-->
- **Insertion sort**

![Insertion sort](https://github.com/hpedrorodrigues/Sort/blob/master/images/InsertionSort.gif)

- **Merge sort**

![Merge sort](https://github.com/hpedrorodrigues/Sort/blob/master/images/MergeSort.gif)

- **Quick sort**

![Quick sort](https://github.com/hpedrorodrigues/Sort/blob/master/images/QuickSort.gif)

<!---
- **Radix sort**

![Radix sort]()
-->
- **Selection sort**

![Selection sort](https://github.com/hpedrorodrigues/Sort/blob/master/images/SelectionSort.gif)

<!---
- **Shell sort**

![Shell sort]()
-->