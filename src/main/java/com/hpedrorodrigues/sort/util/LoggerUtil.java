package com.hpedrorodrigues.sort.util;

import com.hpedrorodrigues.sort.pojo.AlgorithmInfo;

public class LoggerUtil {

    public static void displayResult(int array[], int sortedArray[], long runtime) {
        System.out.print("Array: ");
        for (int item : array) {
            System.out.print(" -> " + item);
        }

        System.out.println();

        System.out.print("Sorted Array: ");
        for (int item : sortedArray) {
            System.out.print(" -> " + item);
        }

        System.out.println("\nRuntime: " + runtime);
    }

    public static void displayMenu() {
        System.out.println("Escolha uma das opções abaixo:");
        System.out.println("0 -> Sair");
        System.out.println("1 -> Selection Sort");
        System.out.println("2 -> Insertion Sort");
        System.out.println("3 -> Bubble Sort");
        System.out.println("4 -> Bubble Sort with flag");
        System.out.println("5 -> Shell Sort");
        System.out.println("6 -> Merge Sort");
        System.out.println("7 -> Quick Sort");
        System.out.println("8 -> Counting Sort");
        System.out.println("9 -> Bucket Sort");
        System.out.println("10 -> Radix Sort");
        System.out.println("11 -> Heap Sort");
    }

    public static void displayAlgorithmResult(AlgorithmInfo algorithmInfo) {
        System.out.println("------------------------------------------------");
        System.out.println(algorithmInfo.getName() + "\n");
        System.out.println("Array aleatório:");
        algorithmInfo.getCallable().call(algorithmInfo.getRandomArray());
        System.out.println("-");
        System.out.println("Array crescente:");
        algorithmInfo.getCallable().call(algorithmInfo.getAscArray());
        System.out.println("-");
        System.out.println("Array decrescente:");
        algorithmInfo.getCallable().call(algorithmInfo.getDescArray());
        System.out.println("------------------------------------------------");
    }
}