package com.hpedrorodrigues.sort.algorithm;

import com.hpedrorodrigues.sort.util.ArrayUtil;

public class BucketSort extends BaseSort {

    private static int[] bucketSort(int[] array) {
        int[] copiedArray = ArrayUtil.copy(array);
        int maxValue = copiedArray[0];

        for (int item : copiedArray) {
            if (item > maxValue) {
                maxValue = item;
            }
        }

        int[] bucket = new int[maxValue + 1];

        for (int i = 0; i < bucket.length; i++) {
            bucket[i] = 0;
        }

        for (int i = 0; i < copiedArray.length; i++) {
            bucket[copiedArray[i]]++;
        }

        int outPos = 0;
        for (int i = 0; i < bucket.length; i++) {
            for (int j = 0; j < bucket[i]; j++) {
                copiedArray[outPos++] = i;
            }
        }

        return copiedArray;
    }

    @Override
    public int[] callback(int[] array) {
        return bucketSort(array);
    }
}