package com.hpedrorodrigues.sort.algorithm;

import com.hpedrorodrigues.sort.util.ArrayUtil;

public class SelectionSort extends BaseSort {

    /**
     * O selection sort (do inglês, ordenação por seleção) é um algoritmo de ordenação baseado em se
     * passar sempre o menor valor do vetor para a primeira posição (ou o maior dependendo da ordem
     * requerida), depois o de segundo menor valor para a segunda posição, e assim é feito
     * sucessivamente com os (n-1) elementos restantes, até os últimos dois elementos.
     *
     * @see <a href="https://pt.wikipedia.org/wiki/Selection_sort">Selection Sort explanation</a>
     * @param array array a ser ordenado
     */
    private int[] selectionSort(int array[]) {
        int copiedArray[] = ArrayUtil.copy(array);
        int n = copiedArray.length, minPos;

        for (int i = 0; i < n - 1; i++) {
            minPos = i;
            for (int j = i + 1; j < n; j++) {
                if (copiedArray[j] < copiedArray[minPos]) {
                    minPos = j;
                }
            }

            if (minPos != i) {
                int aux = copiedArray[i];
                copiedArray[i] = copiedArray[minPos];
                copiedArray[minPos] = aux;
            }
        }

        return copiedArray;
    }

    @Override
    public int[] callback(int[] array) {
        return selectionSort(array);
    }
}