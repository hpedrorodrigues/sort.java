package com.hpedrorodrigues.sort.algorithm;

import com.hpedrorodrigues.sort.util.ArrayUtil;

public class ShellSort extends BaseSort {

    /**
     * Criado por Donald Shell em 1959, publicado pela Universidade de Cincinnati, Shell sort é o
     * mais eficiente algoritmo de classificação dentre os de complexidade quadrática. É um
     * refinamento do método de inserção direta. O algoritmo difere do método de inserção direta
     * pelo fato de no lugar de considerar o array a ser ordenado como um único segmento, ele
     * considera vários segmentos sendo aplicado o método de inserção direta em cada um deles.
     * Basicamente o algoritmo passa várias vezes pela lista dividindo o grupo maior em menores.
     * Nos grupos menores é aplicado o método da ordenação por inserção. Implementações do algoritmo.
     *
     * @see <a href="http://www.guj.com.br/java/183464-shellsort">Shell Sort explanation</a>
     * @param array array a ser ordenado
     */
    private int[] shellSort(int array[]) {
        int copiedArray[] = ArrayUtil.copy(array);
        int n = copiedArray.length, h = 1;

        while (h <= n / 3) {
            h = h * 3 + 1;
        }

        while (h > 0) {
            for (int i = h; i < n; i++) {
                int aux = copiedArray[i];
                int j = i;
                while (j > h - 1 && copiedArray[j - h] >= aux) {
                    copiedArray[j] = copiedArray[j - h];
                    j -= h;
                }
                copiedArray[j] = aux;
            }
            h = (h - 1) / 3;
        }

        return copiedArray;
    }

    @Override
    public int[] callback(int[] array) {
        return shellSort(array);
    }
}