package com.hpedrorodrigues.sort.algorithm;

import com.hpedrorodrigues.sort.util.ArrayUtil;

public class QuickSort extends BaseSort {

    /**
     * O algoritmo Quicksort é um método de ordenação muito rápido e eficiente, inventado por C.A.R.
     * Hoare em 1960, quando visitou a Universidade de Moscovo como estudante. Naquela época, Hoare
     * trabalhou em um projeto de tradução de máquina para o National Physical Laboratory. Ele criou
     * o 'Quicksort ao tentar traduzir um dicionário de inglês para russo, ordenando as palavras,
     * tendo como objetivo reduzir o problema original em sub problemas que possam ser resolvidos
     * mais fácil e rapidamente. Foi publicado em 1962 após uma série de refinamentos.
     * O Quicksort é um algoritmo de ordenação por comparação não-estável.
     *
     * @see <a href="https://pt.wikipedia.org/wiki/Quicksort">Quick Sort explanation</a>
     * @param array array a ser odenado
     */
    private int[] quickSort(int[] array) {
        int[] copiedArray = ArrayUtil.copy(array);
        quickSort(copiedArray, 0, copiedArray.length - 1);
        return copiedArray;
    }

    private void quickSort(int[] array, int start, int end) {
        if (start < end) {
            int pivotPosition = partition(array, start, end);
            quickSort(array, start, pivotPosition - 1);
            quickSort(array, pivotPosition + 1, end);
        }
    }

    private int partition(int[] array, int start, int end) {
        int pivot = array[start];
        int leftPosition = start + 1, rightPosition = end;
        while (leftPosition <= rightPosition) {
            if (array[leftPosition] <= pivot) {
                leftPosition++;
            } else if (pivot < array[rightPosition]) {
                rightPosition--;
            } else {
                int aux = array[leftPosition];
                array[leftPosition] = array[rightPosition];
                array[rightPosition] = aux;
                leftPosition++;
                rightPosition--;
            }
        }
        array[start] = array[rightPosition];
        array[rightPosition] = pivot;
        return rightPosition;
    }


    @Override
    public int[] callback(int[] array) {
        return quickSort(array);
    }
}