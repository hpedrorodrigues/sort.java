package com.hpedrorodrigues.sort.algorithm;

import com.hpedrorodrigues.sort.util.ArrayUtil;

public class BubbleSortWithFlag extends BaseSort {

    private int[] bubbleSortWithFlag(int array[]) {
        int copiedArray[] = ArrayUtil.copy(array);
        int n = copiedArray.length;
        boolean changed = true;

        while (changed) {
            changed = false;
            for (int j = 0;  j < n - 1;  j++) {
                if (copiedArray[j] > copiedArray[j + 1]) {
                    int aux = copiedArray[j];
                    copiedArray[j] = copiedArray[j + 1];
                    copiedArray[j + 1] = aux;
                    changed = true;
                }
            }
        }

        return copiedArray;
    }

    @Override
    public int[] callback(int[] array) {
        return bubbleSortWithFlag(array);
    }
}