package com.hpedrorodrigues.sort.algorithm;

import com.hpedrorodrigues.sort.util.LoggerUtil;
import com.hpedrorodrigues.sort.util.TimeUtil;

public abstract class BaseSort {

    public void sort(int array[]) {
        long startTime = TimeUtil.now();
        int[] sortedArray = callback(array);
        long runtime = TimeUtil.now() - startTime;
        LoggerUtil.displayResult(array, sortedArray, runtime);
    }

    public abstract int[] callback(int array[]);
}