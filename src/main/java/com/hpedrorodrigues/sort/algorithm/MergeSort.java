package com.hpedrorodrigues.sort.algorithm;

import com.hpedrorodrigues.sort.util.ArrayUtil;

public class MergeSort extends BaseSort {

    /**
     * O merge sort, ou ordenação por mistura, ou até mesmo ordenação por entrechechamento, é um
     * exemplo de algoritmo de ordenação do tipo dividir-para-conquistar.
     * Sua ideia básica consiste em Dividir(o problema em vários sub-problemas e resolver esses
     * sub-problemas através da recursividade) e Conquistar(após todos os sub-problemas terem sido
     * resolvidos ocorre a conquista que é a união das resoluções dos sub-problemas). Como o
     * algoritmo do Merge Sort usa a recursividade em alguns problemas esta técnica não é muito
     * eficiente devido ao alto consumo de memória e tempo de execução.
     * Os três passos úteis dos algoritmos dividir-para-conquistar, ou divide and conquer, que se
     * aplicam ao merge sort são:
     *   Dividir: Dividir os dados em subsequências pequenas;
     *   Conquistar: Classificar as duas metades recursivamente aplicando o merge sort;
     *   Combinar: Juntar as duas metades em um único conjunto já classificado.
     *
     * @see <a href="https://pt.wikipedia.org/wiki/Merge_sort">Merge Sort explanation</a>
     * @param array array a ser ordenado
     */
    private int[] mergeSort(int[] array) {
        int copiedArray[] = ArrayUtil.copy(array);
        int n = copiedArray.length;

        if (n <= 1) {
            return copiedArray;
        }

        int half = n / 2;
        int[] right = (n % 2 == 0) ? new int[half] : new int[half + 1];
        int[] left = new int[half];

        for (int i = 0; i < half; i++) {
            left[i] = array[i];
        }

        for (int i = half; i < n; i++) {
            right[i - half] = copiedArray[i];
        }

        left = mergeSort(left);
        right = mergeSort(right);

        return merge(left, right);
    }

    private int[] merge(int[] left, int[] right) {
        int m = left.length, n = right.length, j = 0, i = 0, k = 0;
        int[] result = new int[m + n];

        while (i < m || j < n) {
            if (i < m && j < n) {
                if (left[i] <= right[j]) {
                    result[k] = left[i];
                    k++;
                    i++;
                } else {
                    result[k] = right[j];
                    k++;
                    j++;
                }
            } else if (i < m) {
                result[k] = left[i];
                k++;
                i++;
            } else if (j < n) {
                result[k] = right[j];
                k++;
                j++;
            }
        }
        return result;
    }

    @Override
    public int[] callback(int[] array) {
        return mergeSort(array);
    }
}