package com.hpedrorodrigues.sort.algorithm;

import com.hpedrorodrigues.sort.util.ArrayUtil;

public class HeapSort extends BaseSort {

    private static int[] heapSort(int[] array) {
        int[] copiedArray = ArrayUtil.copy(array);
        int n = copiedArray.length;

        buildMaxHeap(copiedArray);

        for (int i = n - 1; i > 0; i--) {
            swap(copiedArray, i, 0);
            maxHeapify(copiedArray, 0, --n);
        }

        return copiedArray;
    }

    private static void buildMaxHeap(int[] array) {
        int n = array.length;
        for (int i = n / 2 - 1; i >= 0; i--) {
            maxHeapify(array, i, n);
        }
    }

    private static void maxHeapify(int[] array, int position, int arraySize) {

        int max = 2 * position + 1, right = max + 1;

        if (max < arraySize) {
            if (right < arraySize && array[max] < array[right]) {
                max = right;
            }

            if (array[max] > array[position]) {
                swap(array, max, position);
                maxHeapify(array, max, arraySize);
            }
        }
    }

    private static void swap(int[] array, int j, int afterJ) {
        int aux = array[j];
        array[j] = array[afterJ];
        array[afterJ] = aux;
    }

    @Override
    public int[] callback(int[] array) {
        return heapSort(array);
    }
}