package com.hpedrorodrigues.sort.algorithm;

import com.hpedrorodrigues.sort.util.ArrayUtil;

public class CountingSort extends BaseSort {

    /**
     * Counting sort é um algoritmo de ordenação estável cuja complexidade é O(n). As chaves podem
     * tomar valores entre 0 e M-1. Se existirem k0 chaves com valor 0, então ocupam as primeiras
     * k0 posições do vetor final: de 0 a k0-1.
     *
     * 1. Cria cnt[M+1] e b[max N].
     * 2. Inicializa todas as posições de cnt a 0.
     * 3. Percorre o vector a e, para cada posição i de a faz cnt[a[i]-1]++ o que faz com que, no
     *    final, cada posição i de cnt contem o nº de vezes que a chave i-1 aparece em a.
     * 4. Acumula em cada elemento de cnt o elemento somado ao elemento anterior: cnt[i] indica a
     *    posição ordenada do primeiro elemento de chave i.
     * 5. Guarda em b os valores de a ordenados de acordo com b[cnt[a[i]++]=a[i].
     * 6. Copia b para a.
     * 7. Counting-Sort trabalha como uma contadora de ocorrências dentro de um programa,
     *    especificamente dentro de um vetor. Quando determinado vetor tem números repetidos,
     *    números únicos e números que não existem um outro vetor indica a quantidade de
     *    ocorrências.
     *
     * Esta implementação tem a desvantagem de precisar de vectores auxiliares. O Counting Sort
     * ordena exclusivamente números inteiros pelo fato de seus valores servirem como índices no
     * vetor de contagem.
     *
     * @see <a href="https://pt.wikipedia.org/wiki/Counting_sort">Counting Sort explanation</a>
     * @param array array a ser ordenado
     */
    private int[] countingSort(int[] array) {
        int[] copiedArray = ArrayUtil.copy(array);
        int n = copiedArray.length;

        int maxValue = copiedArray[0];
        for (int item : copiedArray) {
            if (item > maxValue) {
                maxValue = item;
            }
        }
        maxValue++;

        int aux[] = new int[maxValue];

        //1ª - (Inicializar com zero)
        for (int i = 0; i < maxValue; i++) {
            aux[i] = 0;
        }

        //2ª - Contagem das ocorrências
        for (int i = 0; i < n; i++) {
            aux[copiedArray[i]]++;
        }

        //3ª - Ordenando os indices do vetor auxiliar
        int sum = 0;
        for (int i = 1; i < maxValue; i++) {
            int auxValue = aux[i];
            aux[i] = sum;
            sum += auxValue;
        }
        int sortedArray[] = new int[n];
        for (int i = 0; i < n; i++) {
            sortedArray[aux[copiedArray[i]]] = copiedArray[i];
            aux[copiedArray[i]]++;
        }

        //4ª Retornando os valores ordenados para o vetor de entrada
        for (int i = 0; i < n; i++) {
            copiedArray[i] = sortedArray[i];
        }

        return copiedArray;
    }

    @Override
    public int[] callback(int[] array) {
        return countingSort(array);
    }
}