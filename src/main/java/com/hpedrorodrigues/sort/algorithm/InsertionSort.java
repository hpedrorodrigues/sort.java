package com.hpedrorodrigues.sort.algorithm;

import com.hpedrorodrigues.sort.util.ArrayUtil;

public class InsertionSort extends BaseSort {

    /**
     * Insertion sort, ou ordenação por inserção, é um simples algoritmo de ordenação, eficiente
     * quando aplicado a um pequeno número de elementos. Em termos gerais, ele percorre um vetor de
     * elementos da esquerda para a direita e à medida que avança vai deixando os elementos mais à
     * esquerda ordenados. O algoritmo de inserção funciona da mesma maneira com que muitas pessoas
     * ordenam cartas em um jogo de baralho como o pôquer.
     *
     * @see <a href="https://pt.wikipedia.org/wiki/Insertion_sort">Insertion Sort explanation</a>
     * @param array array a ser ordenado
     */
    private int[] insertionSort(int array[]) {
        int copiedArray[] = ArrayUtil.copy(array);
        int n = copiedArray.length;

        for (int i = 1; i < n; i++) {
            int j = i - 1;
            while (j >= 0 && copiedArray[j] > copiedArray[j + 1]) {
                int aux = copiedArray[j];
                copiedArray[j] = copiedArray[j + 1];
                copiedArray[j + 1] = aux;
                j--;
            }
        }

        return copiedArray;
    }

    @Override
    public int[] callback(int[] array) {
        return insertionSort(array);
    }
}