package com.hpedrorodrigues.sort;

import com.hpedrorodrigues.sort.algorithm.BaseSort;
import com.hpedrorodrigues.sort.algorithm.ShellSort;

public class ShellSortTest extends BaseSortTest {

    ShellSort shellSort = new ShellSort();

    @Override
    protected BaseSort getAlgorithm() {
        return shellSort;
    }
}