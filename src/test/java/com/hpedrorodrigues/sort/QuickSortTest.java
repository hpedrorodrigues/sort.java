package com.hpedrorodrigues.sort;

import com.hpedrorodrigues.sort.algorithm.BaseSort;
import com.hpedrorodrigues.sort.algorithm.QuickSort;

public class QuickSortTest extends BaseSortTest {

    QuickSort quickSort = new QuickSort();

    @Override
    protected BaseSort getAlgorithm() {
        return quickSort;
    }
}