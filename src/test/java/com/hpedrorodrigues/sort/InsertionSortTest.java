package com.hpedrorodrigues.sort;

import com.hpedrorodrigues.sort.algorithm.BaseSort;
import com.hpedrorodrigues.sort.algorithm.InsertionSort;

public class InsertionSortTest extends BaseSortTest {

    InsertionSort insertionSort = new InsertionSort();

    @Override
    protected BaseSort getAlgorithm() {
        return insertionSort;
    }
}