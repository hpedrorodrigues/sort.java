package com.hpedrorodrigues.sort;

import com.hpedrorodrigues.sort.algorithm.BaseSort;
import com.hpedrorodrigues.sort.algorithm.BubbleSortWithFlag;

public class BubbleSortWithFlagTest extends BaseSortTest {

    BubbleSortWithFlag bubbleSortWithFlag = new BubbleSortWithFlag();

    @Override
    protected BaseSort getAlgorithm() {
        return bubbleSortWithFlag;
    }
}