package com.hpedrorodrigues.sort;

import com.hpedrorodrigues.sort.algorithm.BaseSort;
import com.hpedrorodrigues.sort.algorithm.CountingSort;

public class CountingSortTest extends BaseSortTest {

    CountingSort countingSort = new CountingSort();

    @Override
    protected BaseSort getAlgorithm() {
        return countingSort;
    }
}