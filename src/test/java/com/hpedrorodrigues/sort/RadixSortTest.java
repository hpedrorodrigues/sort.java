package com.hpedrorodrigues.sort;

import com.hpedrorodrigues.sort.algorithm.BaseSort;
import com.hpedrorodrigues.sort.algorithm.RadixSort;

public class RadixSortTest extends BaseSortTest {

    RadixSort radixSort = new RadixSort();

    @Override
    protected BaseSort getAlgorithm() {
        return radixSort;
    }
}