package com.hpedrorodrigues.sort;

import com.hpedrorodrigues.sort.algorithm.BaseSort;
import com.hpedrorodrigues.sort.algorithm.SelectionSort;

public class SelectionSortTest extends BaseSortTest {

    SelectionSort selectionSort = new SelectionSort();

    @Override
    protected BaseSort getAlgorithm() {
        return selectionSort;
    }
}