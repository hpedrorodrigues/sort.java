package com.hpedrorodrigues.sort;

import com.hpedrorodrigues.sort.algorithm.BaseSort;
import com.hpedrorodrigues.sort.algorithm.MergeSort;

public class MergeSortTest extends BaseSortTest {

    MergeSort mergeSort = new MergeSort();

    @Override
    protected BaseSort getAlgorithm() {
        return mergeSort;
    }
}