package com.hpedrorodrigues.sort;

import com.hpedrorodrigues.sort.algorithm.BaseSort;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public abstract class BaseSortTest {

    protected static final int[] RANDOM_ARRAY = new int[] {5, 3, 1, 2, 4};
    protected static final int[] ASC_ARRAY = new int[] {1, 2, 3, 4, 5};
    protected static final int[] DESC_ARRAY = new int[] {5, 4, 3, 2, 1};
    protected static final int[] SORTED_ARRAY = ASC_ARRAY;

    protected abstract BaseSort getAlgorithm();

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("Tests running :)");
    }

    @AfterClass
    public static void oneTimeTearDown() {
        System.out.println("Tests finished :)");
    }

    @Test
    public void testRandomArray() {
        Assert.assertArrayEquals(SORTED_ARRAY, getAlgorithm().callback(RANDOM_ARRAY));
    }

    @Test
    public void testAscArray() {
        Assert.assertArrayEquals(SORTED_ARRAY, getAlgorithm().callback(ASC_ARRAY));
    }

    @Test
    public void testDescArray() {
        Assert.assertArrayEquals(SORTED_ARRAY, getAlgorithm().callback(DESC_ARRAY));
    }
}