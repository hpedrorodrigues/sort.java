package com.hpedrorodrigues.sort;

import com.hpedrorodrigues.sort.algorithm.BaseSort;
import com.hpedrorodrigues.sort.algorithm.HeapSort;

public class HeapSortTest extends BaseSortTest {

    HeapSort heapSort = new HeapSort();

    @Override
    protected BaseSort getAlgorithm() {
        return heapSort;
    }
}