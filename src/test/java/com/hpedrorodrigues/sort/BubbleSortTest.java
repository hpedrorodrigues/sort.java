package com.hpedrorodrigues.sort;

import com.hpedrorodrigues.sort.algorithm.BaseSort;
import com.hpedrorodrigues.sort.algorithm.BubbleSort;

public class BubbleSortTest extends BaseSortTest {

    BubbleSort bubbleSort = new BubbleSort();

    @Override
    protected BaseSort getAlgorithm() {
        return bubbleSort;
    }
}