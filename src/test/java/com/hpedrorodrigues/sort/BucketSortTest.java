package com.hpedrorodrigues.sort;

import com.hpedrorodrigues.sort.algorithm.BaseSort;
import com.hpedrorodrigues.sort.algorithm.BucketSort;

public class BucketSortTest extends BaseSortTest {

    BucketSort bucketSort = new BucketSort();

    @Override
    protected BaseSort getAlgorithm() {
        return bucketSort;
    }
}