# Sort

A School Work.

Some data sorting algorithms.

### Algorithms

- Bubble sort
- Bubble sort with flag
- Bucket sort
- Counting sort
- Heap sort
- Insertion sort
- Merge sort
- Quick sort
- Radix sort
- Selection sort
- Shell sort

See [images](https://github.com/hpedrorodrigues/Sort/blob/master/IMAGES.md) for more details.

### Simple Commands

Command used to create this project:
```bash
mvn archetype:generate 
  -DgroupId=com.hpedrorodrigues.sort 
  -DartifactId=Sort 
  -DarchetypeArtifactId=maven-archetype-quickstart 
  -DinteractiveMode=false
```

Command to get the compiled code and package it into its distribution format, as JAR:
```bash
mvn package
```

Command to install the package into the local repository, for use as a dependency in other projects 
locally:
```bash
mvn install
```

Command to run the jar file:
```bash
java -jar target/Sort-1.0-SNAPSHOT.jar
```

or

```bash
mvn exec:java -Dexec.mainClass="com.hpedrorodrigues.sort.app.Runner"
```

Command to run the tests:
```bash
mvn test
```
